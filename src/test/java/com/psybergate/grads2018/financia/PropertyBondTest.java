package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.PropertyBond;

public class PropertyBondTest {

	@Test
	public void shouldPassWhenMonhtlyRepaymentIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		// then
		assertEquals(7157.06, bond.getRepayment().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenTotalMonhtlyRepaymentIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.getForecasts();
		// then
		assertEquals(1717694.4, bond.getTotalRepayment().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenTotalInterestEarnedIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.getForecasts();
		// then
		assertEquals(1067694.04, bond.getTotalInterest().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenBondCostIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.getForecasts();
		// then
		assertEquals(7500.0, bond.getBondCost().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenLegalCostIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.getForecasts();
		// then
		assertEquals(9000.0, bond.getLegalCost().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenTranserCostIsEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.getForecasts();
		// then
		assertEquals(0.0, bond.getTransferCost().getAmount(), 0.9);
	}

	@Test
	public void shouldPassWhenBondsAreEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond1 = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		PropertyBond bond2 = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		// then
		assertTrue(bond1.equals(bond2));
	}

	@Test
	public void shouldPassWhenBondsAreNotEqual() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond1 = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond1.addEvent(new Event(11, 3000.3, EventType.DEPOSIT));
		bond1.getForecasts();
		PropertyBond bond2 = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond2.getForecasts();
		// then
		assertFalse(bond1.equals(bond2));
	}

	@Test
	public void shouldPassWhenBondForecastSizeIsEqualTo() {
		// given
		Interest interest = new Interest(12.0);
		Money purchasePrice = new Money(750_000.0);
		Money deposit = new Money(100_000.0);
		Integer term = 240;
		// when
		PropertyBond bond1 = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		// then
		assertTrue(bond1.getForecasts().size() == 240);
	}

}
