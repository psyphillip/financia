package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.MonthlyInvestment;

public class MonthlyInvestmentWithEventsTest {

	private static final double DELTA = 0.9;

	@Test
	public void totalFinalAmount() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		Event withdraw = new Event(11, 10000.0, EventType.WITHDRAW);
		Event deposit = new Event(15, 20000.0, EventType.DEPOSIT);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.addEvent(withdraw);
		investemnt.addEvent(deposit);
		investemnt.getForecasts();
		Money totalFinalAmount = investemnt.getFinalClosingAmount();
		// then
		assertEquals(132580.29, totalFinalAmount.getAmount(), DELTA);
	}

	@Test
	public void totalFinalAmountWithInterestChange() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		Event withdraw = new Event(11, 10000.0, EventType.WITHDRAW);
		Event deposit = new Event(15, 20000.0, EventType.DEPOSIT);
		Event changeInterest = new Event(31, 18.0, EventType.CHANGE_INTEREST);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.addEvent(withdraw);
		investemnt.addEvent(deposit);
		investemnt.addEvent(changeInterest);
		investemnt.getForecasts();
		Money totalFinalAmount = investemnt.getFinalClosingAmount();
		// then
		assertEquals(120764.71, totalFinalAmount.getAmount(), DELTA);
	}

	@Test
	public void totalFinalAmountWithAmountChange() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		Event withdraw = new Event(11, 10000.0, EventType.WITHDRAW);
		Event deposit = new Event(15, 20000.0, EventType.DEPOSIT);
		Event changeInterest = new Event(31, 18.0, EventType.CHANGE_INTEREST);
		Event changeAmount = new Event(22, 3000.0, EventType.CHANGE_AMOUNT);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.addEvent(withdraw);
		investemnt.addEvent(deposit);
		investemnt.addEvent(changeInterest);
		investemnt.addEvent(changeAmount);
		Money totalFinalAmount = investemnt.getFinalClosingAmount();
		// then
		assertEquals(227864.72, totalFinalAmount.getAmount(), DELTA);
	}

	@Test
	public void totalInteretsEarned() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		Event withdraw = new Event(11, 10000.0, EventType.WITHDRAW);
		Event deposit = new Event(15, 20000.0, EventType.DEPOSIT);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.addEvent(deposit);
		investemnt.addEvent(withdraw);
		Money totalInterest = investemnt.getTotalInterestEarned();
		// then
		assertEquals(62580.29, totalInterest.getAmount(), DELTA);
	}

	@Test
	public void totalMonthlyContribution() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.getForecasts();
		Money totalContributions = ((MonthlyInvestment) investemnt).getTotalContribution();
		// then
		assertEquals(60000, totalContributions.getAmount(), DELTA);
	}

	@Test
	public void numberOfForecasts() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		int numberOfForecasts = investemnt.getForecasts().size();
		// then
		assertEquals(60, numberOfForecasts);
	}

}
