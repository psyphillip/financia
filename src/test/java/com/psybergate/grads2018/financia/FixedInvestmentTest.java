package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.FixedInvestment;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.entities.Money;

public class FixedInvestmentTest {

	private static final double DELTA = 0.09;

	private Investment investemnt = new FixedInvestment(20, new Interest(7.3), new Money(3000.0));

	@Test
	public void totalFinalAmount() {
		// given
		Integer term = 60;
		Money money = new Money(1000000.0);
		Interest interest = new Interest(8.0);
		// when
		Investment investemnt = new FixedInvestment(term, interest, money);
		investemnt.getForecasts();

		Money totalFinalAmount = investemnt.getFinalClosingAmount();
		// then
		assertEquals(1489845.71, totalFinalAmount.getAmount(), DELTA);
	}

	@Test
	public void totalInteretsEarned() {
		// given
		Integer term = 60;
		Money money = new Money(1000000.0);
		Interest interest = new Interest(8.0);
		// when
		Investment investemnt = new FixedInvestment(term, interest, money);
		investemnt.getForecasts();
		Money totalInterest = investemnt.getTotalInterestEarned();
		// then
		assertEquals(489845.71, totalInterest.getAmount(), DELTA);
	}

	@Test
	public void shouldPassWhenInvementsAreEqual() {
		Investment investment = new FixedInvestment(20, new Interest(7.3), new Money(3000.0));
		assertTrue(investemnt.equals(investment));
	}

	@Test
	public void shouldPassWhenInvementsAreNotEqual() {
		Investment investemnt = new FixedInvestment(20, new Interest(7.3), new Money(3000.0));
		investemnt.addEvent(new Event(12,3.5, EventType.CHANGE_INTEREST));
		assertFalse(this.investemnt.equals(investemnt));
	}

}
