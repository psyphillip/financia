package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.MonthlyInvestment;

public class MonthlyInvestmentTest {

	private static final double DELTA = 0.09;

	@Test
	public void totalFinalAmount() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.getForecasts();
		Money totalFinalAmount = investemnt.getFinalClosingAmount();
		// then
		assertEquals(110964.04, totalFinalAmount.getAmount(), DELTA);
	}

	@Test
	public void totalInteretsEarned() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.getForecasts();
		Money totalInterest = investemnt.getTotalInterestEarned();
		// then
		assertEquals(50964.04, totalInterest.getAmount(), DELTA);
	}

	@Test
	public void totalMonthlyContribution() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		investemnt.getForecasts();
		Money totalContributions = ((MonthlyInvestment) investemnt).getTotalContribution();
		// then
		assertEquals(60000, totalContributions.getAmount(), DELTA);
	}

	@Test
	public void numberOfForecasts() {
		// given
		Integer term = 60;
		Money monthlyDeposit = new Money(1000.0);
		Interest interest = new Interest(22.4);
		// when
		Investment investemnt = new MonthlyInvestment(term, interest, monthlyDeposit);
		int numberOfForecasts = investemnt.getForecasts().size();
		// then
		assertEquals(60, numberOfForecasts);
	}

}
