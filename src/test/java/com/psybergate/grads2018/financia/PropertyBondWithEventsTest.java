package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.PropertyBond;

public class PropertyBondWithEventsTest {

	@Test
	public void shouldPassWhenAmountIsWithdrawn() {
		// given
		Interest interest = new Interest(11.0);
		Money purchasePrice = new Money(700_000.0);
		Money deposit = new Money(0.0);
		Integer term = 240;
		Event fixedAmount = new Event(1,3000.0,EventType.ADD_FIXED_REPAYMENT);
		// when
		PropertyBond bond = new PropertyBond(8372983L, purchasePrice, term, interest, deposit);
		bond.addEvent(fixedAmount);
		bond.getForecasts();
		// then
    assertEquals(720_000.0, bond.getTotalRepayment().getAmount(), 0.9);

	}
}
