package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.FixedInvestment;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.entities.Money;

public class FixedInvestmentWithEventsTest {

	@Test
	public void totalWithdrawlAmount() {
		// given
		Integer term = 60;
		Money money = new Money(1000000.0);
		Interest interest = new Interest(8.0);
		Event withdraw = new Event(3, 3000.0, EventType.WITHDRAW);
		Event deposit = new Event(7, 4000.0, EventType.WITHDRAW);
		// when
		Investment investemnt = new FixedInvestment(term, interest, money);
		investemnt.addEvent(deposit);
		investemnt.addEvent(withdraw);
		investemnt.getForecasts();
		Money totalWithdrawl = investemnt.getTotalWithdrawnAmount();
		// then
		assertEquals(7000, totalWithdrawl.getAmount(), 0.01);
	}

	@Test
	public void totalDepositAmount() {
		// given
		Integer term = 60;
		Money money = new Money(1000000.0);
		Interest interest = new Interest(8.0);
		Event withdraw = new Event(3, 3000.0, EventType.WITHDRAW);
		Event deposit = new Event(7, 4000.0, EventType.WITHDRAW);
		// when
		Investment investemnt = new FixedInvestment(term, interest, money);
		investemnt.addEvent(deposit);
		investemnt.addEvent(withdraw);
		investemnt.getForecasts();
		Money totalDeposits = investemnt.getTotalDepositedAmount();
		// then
		assertEquals(0, totalDeposits.getAmount(), 0.1);
	}

	@Test
	public void shouldPassWhenInterestIsChanged() {
		// given
		Integer term = 60;
		Money money = new Money(1000000.0);
		Interest interest = new Interest(8.0);
		Event changeInterest = new Event(3, 3.8, EventType.CHANGE_INTEREST);
		// when
		Investment investemnt = new FixedInvestment(term, interest, money);
		investemnt.addEvent(changeInterest);
		investemnt.getForecasts();
		// then
		assertFalse(3.8 == investemnt.getInterest().getInterestRate());
	}
 
}
