package com.psybergate.grads2018.financia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;

public class EventTest {

	private Event deposit;

	private Event withdraw;

	private Event changeAmount;

	private Event changeInterest;

	private Event addFixedAmount;
 
	@Before
	public void inilialize() {
		deposit = new Event(3, 3000.0, EventType.DEPOSIT);
		withdraw = new Event(3, 3000.0, EventType.WITHDRAW);
		changeAmount = new Event(3, 3000.0, EventType.CHANGE_AMOUNT);
		addFixedAmount = new Event(3, 7000.0, EventType.ADD_FIXED_REPAYMENT);
	}

	@Test
	public void shouldPassWhenEventsAreEqual() {
		Event anotherDeposit = new Event(3, 9000.0, EventType.DEPOSIT);
		assertTrue(this.deposit.equals(anotherDeposit));
		assertTrue(addFixedAmount.equals(addFixedAmount));
		assertEquals(3000, withdraw.getValue(), 0.1);
	}

	@Test
	public void shouldPassWhenEventsAreNotEqual() {
		assertFalse(withdraw.equals(null));
		assertFalse(deposit.equals(changeAmount));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionWhenNullObjectMethodIsInvoked() {
		Event anotherEvent = new Event();
		assertNull(changeInterest);
		assertNotNull(anotherEvent);
		System.out.println(changeInterest.equals(anotherEvent));
	}
 
}
