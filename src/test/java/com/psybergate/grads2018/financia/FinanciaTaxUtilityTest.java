package com.psybergate.grads2018.financia;

import org.junit.Assert;
import org.junit.Test;

import com.psybergate.grads2018.financia.utilities.FinanciaTaxUtility;

public class FinanciaTaxUtilityTest {
 
	private final int PROPERTY1_PRICE = 500_000;
	private final int PROPERTY2_PRICE = 1_000_000;
	private final int PROPERTY3_PRICE = 1_500_000;
	private final int PROPERTY4_PRICE = 2_000_000;
	private final int PROPERTY5_PRICE = 5_000_000;
	private final int PROPERTY6_PRICE = 15_000_000;
	
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY1 = 0.0;
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY2 = 3_000.0;
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY3 = 25_500.0;
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY4 = 60_500.00;
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY5 = 383_000.00;
	private final Double EXPECTED_TRANSFER_TAX_FOR_PROPERTY6 = 1_583_000.00;

	@Test
	public void testFirstBracket() {
		Assert.assertEquals("Property 1 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY1, FinanciaTaxUtility.calculateTransferRates(PROPERTY1_PRICE));
	}

	@Test
	public void testSecondBracket() {
		Assert.assertEquals("Property 2 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY2, FinanciaTaxUtility.calculateTransferRates(PROPERTY2_PRICE));
	}

	@Test
	public void testThirdBracket() {
		Assert.assertEquals("Property 3 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY3, FinanciaTaxUtility.calculateTransferRates(PROPERTY3_PRICE));
	}

	@Test
	public void testFourthBracket() {
		Assert.assertEquals("Property 4 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY4, FinanciaTaxUtility.calculateTransferRates(PROPERTY4_PRICE));
	}

	@Test
	public void testFithBracket() {
		Assert.assertEquals("Property 5 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY5, FinanciaTaxUtility.calculateTransferRates(PROPERTY5_PRICE));
	}
	
	@Test
	public void testSixthBracket() {
		Assert.assertEquals("Property 6 tax incorrect", EXPECTED_TRANSFER_TAX_FOR_PROPERTY6, FinanciaTaxUtility.calculateTransferRates(PROPERTY6_PRICE));
	}

}
