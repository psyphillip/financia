package com.psybergate.grads2018.financia.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.FixedInvestment;
import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.MonthlyInvestment;
import com.psybergate.grads2018.financia.exceptions.FinanciaException;
import com.psybergate.grads2018.financia.services.InvestmentService;
import com.psybergate.grads2018.financia.services.PropertyBondService;

@ManagedBean(value = "InvestmentController")
public class InvestmentController {

	@EJB
	private InvestmentService investmentService;

	@EJB
	private PropertyBondService propertyBondService;

	public String getAllInvestments(HttpServletRequest request) {
		try {
			setBondAndInvestmentList(request);
			return "/WEB-INF/views/index.jsp";
		}
		catch (Exception e) {
			return "/WEB-INF/views/error.jsp";
		}
	}

	public String saveInvestment(HttpServletRequest req) {
		Investment investment = createInvestmentWithEvents(req);
		try {
			investmentService.saveInvestment(investment);
			setMessage(req, "Investment successfully saved");
			return generateForecast(req);
		}
		catch (Exception e) {
			if (e.getCause() instanceof FinanciaException) {
				setMessage(req, e.getCause().getMessage());
				setAttributes(req, investment);
				return generateForecast(req);
			}
			else {
				return "/WEB-INF/views/error.jsp";
			}
		}

	}

	public String removeInvestment(HttpServletRequest req) {
		Long code = Long.parseLong(req.getParameter("code"));
		try {
			Investment investment = investmentService.getInvestmentByCode(code);
			investmentService.removeInvestment(investment);
			setBondAndInvestmentList(req);
			setMessage(req, "Investment successfully removed");
			return "/WEB-INF/views/index.jsp";
		}
		catch (Exception e) {
			return "/WEB-INF/views/error.jsp";
		}
	}

	public String removeEvent(HttpServletRequest req) {
		Investment investment = createInvestmentWithEvents(req);
		Event eventToBeRemoved = recreateEvent(req);
		try {
			investmentService.removeEvent(investment, eventToBeRemoved);
			List<Forecast> forecasts = investmentService.generateForecast(investment);
			req.setAttribute("forecasts", forecasts);
			setAttributes(req, investment);
		}
		catch (Exception e) {
			return "/WEB-INF/views/error.jsp";
		}
		return "/WEB-INF/views/investment_forecast.jsp";
	}

	public String generateForecast(HttpServletRequest req) {

		String type = req.getParameter("type");
		Investment investment = null;
		List<Forecast> forecasts = null;

		try {
			if (type == null) {
				investment = getInvesment(req);
			}
			else {
				investment = createInvestment(req);
				List<Event> events = getExistingEvents(req);
				investment.setEvents(events);
			}
			forecasts = investmentService.generateForecast(investment);
		}
		catch (Exception e) {
			return "/WEB-INF/views/error.jsp";
		}
		setAttributes(req, investment);
		req.setAttribute("forecasts", forecasts);
		return "/WEB-INF/views/investment_forecast.jsp";
	}

	public String addEvent(HttpServletRequest req) {
		Investment investment = createInvestmentWithEvents(req);
		Event event = createEvent(req);
		try {
			investmentService.addEvent(investment, event);
		}
		catch (Exception e) {
			setMessage(req, e.getCause().getMessage());
		}
		setAttributes(req, investment);
		req.setAttribute("forecasts", investment.getForecasts());
		return "/WEB-INF/views/investment_forecast.jsp";
	}

	private List<Event> getExistingEvents(HttpServletRequest req) {
		List<Event> events = new ArrayList<>();
		String[] months = req.getParameterMap().get("monthName");
		String[] types = req.getParameterMap().get("eventName");
		String[] values = req.getParameterMap().get("valueName");
		if (months != null) {
			for (int i = 0; i < values.length; i++) {
				Event event = new Event(Integer.parseInt(months[i]), removeCommas(values[i]), getEventType(types[i]));
				events.add(event);
			}
		}
		return events;
	}

	private Event createEvent(HttpServletRequest req) {
		if (req.getParameter("eventType") != null) {
			if (req.getParameter("eventValue").equals("") || req.getParameter("eventMonth").equals("")) {
				return null;
			}
			else {
				String eventType = req.getParameter("eventType");
				Integer eventMonth = Integer.parseInt(req.getParameter("eventMonth"));
				Double eventValue = Double.parseDouble(req.getParameter("eventValue"));
				EventType type = getEventType(eventType);
				return new Event(eventMonth, eventValue, type);
			}

		}
		return null;
	}

	private Event recreateEvent(HttpServletRequest req) {
		String eventType = req.getParameter("eventName");
		Integer eventMonth = Integer.parseInt(req.getParameter("monthName"));
		Double eventValue = removeCommas(req.getParameter("valueName"));
		EventType type = getEventType(eventType);
		return new Event(eventMonth, eventValue, type);
	}

	private void setMessage(HttpServletRequest req, String message) {
		req.setAttribute("message", message);
	}

	private void setBondAndInvestmentList(HttpServletRequest req) {
		req.setAttribute("investments", investmentService.getAllInvestments());
		req.setAttribute("bonds", propertyBondService.getAllPropertyBonds());
	}

	private EventType getEventType(String eventType) {
		if (eventType.toLowerCase().equals("withdraw")) {
			return EventType.WITHDRAW;
		}
		if (eventType.toLowerCase().equals("deposit")) {
			return EventType.DEPOSIT;
		}
		if (eventType.toLowerCase().equals("change amount") || eventType.toLowerCase().equals("change_amount")) {
			return EventType.CHANGE_AMOUNT;
		}
		if (eventType.toLowerCase().equals("change interest") || eventType.toLowerCase().equals("change_interest")) {
			return EventType.CHANGE_INTEREST;
		}
		return null;
	}

	private Investment getInvesment(HttpServletRequest req) {
		Long code = Long.parseLong(req.getParameter("code"));
		return investmentService.getInvestmentByCode(code);
	}

	private void setAttributes(HttpServletRequest req, Investment investment) {
		// req.setAttribute("type", investment.getInvementType());
		List<Event> events = investment.getEvents();
		if (investment.getInvestmentType().equals("FixedInvestment")) {
			removeChangeAmountEvent(events);
			req.setAttribute("type", "Fixed");
			req.setAttribute("totalContributions", "N/A");
		}
		else {
			req.setAttribute("type", "Monthly");
			req.setAttribute("totalContributions", ((MonthlyInvestment) investment).getTotalContribution().toString());
		}
		req.setAttribute("code", investment.getCode());
		req.setAttribute("term", investment.getTerm());
		Double amount = removeCommas(investment.getAmount().toString());
		req.setAttribute("amount", new Money(amount).getAmount());
		req.setAttribute("interest", investment.getInterest().toString());
		req.setAttribute("events", events);
		req.setAttribute("totalDeposit", investment.getTotalDepositedAmount().toString());
		req.setAttribute("totalWithdrawals", investment.getTotalWithdrawnAmount().toString());
		req.setAttribute("totalInterest", investment.getTotalInterestEarned().toString());
		req.setAttribute("finalClosingBalance", investment.getFinalClosingAmount().toString());

	}

	private void removeChangeAmountEvent(List<Event> events) {
		for (int r = 0; r < events.size(); r++) {
			if (events.get(r).getEvent().equals(EventType.CHANGE_AMOUNT)) {
				events.remove(events.get(r));
			}
		}
	}

	private Long generateNumber(int i) {
		return (long) (Math.random() * i);
	}

	private Investment createInvestmentWithEvents(HttpServletRequest req) {
		// Investment attributes
		Investment investment = createInvestment(req);
		List<Event> events = getExistingEvents(req);
		if (investment.getInvestmentType().equals("FixedInvestment")) {
			removeChangeAmountEvent(events);
		}
		investment.setEvents(events);
		return investment;
	}

	private Investment createInvestment(HttpServletRequest req) {
		Long code = generateNumber(1000000);
		Integer term = Integer.parseInt(req.getParameter("term"));
		Double interest = Double.parseDouble(req.getParameter("interest"));
		Double amount = Double.parseDouble(req.getParameter("amount"));
		String type = String.valueOf(req.getParameter("type"));
		// Check investment type
		if (type.equals("Fixed")) {
			return new FixedInvestment(code, term, new Interest(interest), new Money(amount));
		}
		else {
			return new MonthlyInvestment(code, term, new Interest(interest), new Money(amount));
		}
	}

	private Double removeCommas(String amount) {
		Double newamount = 0.0;
		if (amount.contains(",")) {
			newamount = Double.parseDouble(amount.replace(",", ""));
			return newamount;
		}
		return Double.parseDouble(amount);
	}
}