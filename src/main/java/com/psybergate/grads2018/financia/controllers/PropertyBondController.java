package com.psybergate.grads2018.financia.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.EventType;
import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.Interest;
import com.psybergate.grads2018.financia.entities.Money;
import com.psybergate.grads2018.financia.entities.PropertyBond;
import com.psybergate.grads2018.financia.exceptions.FinanciaException;
import com.psybergate.grads2018.financia.services.InvestmentService;
import com.psybergate.grads2018.financia.services.PropertyBondService;

@ManagedBean(value = "PropertyBondController")
public class PropertyBondController {

	@EJB
	private PropertyBondService propertyBondService;

	@EJB 
	InvestmentService investmentService;

	public String savePropertyBond(HttpServletRequest req) {
		PropertyBond propertyBond = createPropertyBond(req);
		try {
			List<Event> events = createEvents(req);
			if (events.size() > 0) {
				for (Event event2 : events) {
					propertyBond.addEvent(event2);
				}
			}
			propertyBondService.savePropertyBond(propertyBond);
			req.setAttribute("message", "Property bond successfully saved");
			return generatePropertyBondForecast(req);
		}
		catch (Exception e) {
			if (e.getCause() instanceof FinanciaException) {
				req.setAttribute("message", propertyBondService.getFieldMessageError(propertyBond));
				setAttributes(req, propertyBond);
				return generatePropertyBondForecast(req);
			} else {
				return "/WEB-INF/views/error.jsp";
			}
		}
	}

	private List<Event> createEvents(HttpServletRequest req) {
		String[] months = req.getParameterMap().get("monthName");
		String[] types = req.getParameterMap().get("eventName");
		String[] values = req.getParameterMap().get("valueName");
		List<Event> events = new ArrayList<>();
		if (months != null) {
			for (int i = 0; i < values.length; i++) {
				events.add(new Event(Integer.parseInt(months[i]), removeCommas(values[i]), getEventType(types[i])));
			}
		}
		return events;
	}

	private EventType getEventType(String eventType) {
		if (eventType.toLowerCase().equals("withdraw")) {
			return EventType.WITHDRAW;
		}
		if (eventType.toLowerCase().equals("deposit")) {
			return EventType.DEPOSIT;
		}
		if (eventType.toLowerCase().equals("add fixed repayment")
				|| eventType.toLowerCase().equals("add_fixed_repayment")) {
			return EventType.ADD_FIXED_REPAYMENT;
		}
		if (eventType.toLowerCase().equals("change interest") || eventType.toLowerCase().equals("change_interest")) {
			return EventType.CHANGE_INTEREST;
		}
		return null;
	}

	public String removeEvent(HttpServletRequest req) {
		PropertyBond propertyBond = createPropertyBond(req);
		Event eventToBeRemoved = recreateEvent(req);
		List<Event> events = createEvents(req);
		for (Event event : events) {
			if (!eventToBeRemoved.equals(event)) {
				System.out.println("no match yet.");
				propertyBond.addEvent(event);
			}else {
				System.out.println("we found the match.");
			}
		}
		List<Forecast> forecasts = propertyBondService.generateForecast(propertyBond);
		req.setAttribute("forecasts", forecasts);
		setAttributes(req, propertyBond);
		return "/WEB-INF/views/bond_forecast.jsp";
	}
	
	private Event recreateEvent(HttpServletRequest req) {
		String eventType = req.getParameter("eventName");
		Integer eventMonth = Integer.parseInt(req.getParameter("monthName"));
		Double eventValue = removeCommas(req.getParameter("valueName"));
		EventType type = getEventType(eventType);
		return new Event(eventMonth, eventValue, type);
	}

	private void setAttributes(HttpServletRequest req, PropertyBond propertyBond) {
		req.setAttribute("price", removeCommas(propertyBond.getPropertyPrice().toString()));
		req.setAttribute("term", propertyBond.getTerm());
		req.setAttribute("deposit", removeCommas(propertyBond.getDeposit().toString()));
		req.setAttribute("interest", propertyBond.getRate().toString());
		req.setAttribute("repayment", propertyBond.getRepayment().toString());
		req.setAttribute("events", propertyBond.getEvents());
		req.setAttribute("bondTime", propertyBond.getBondRepaymentTime());
		req.setAttribute("outstanding", propertyBond.getOutstandingAmount().toString());
		req.setAttribute("propertyPrice", propertyBond.getPropertyPrice().toString());
		req.setAttribute("totalRepayment", propertyBond.getTotalRepayment().toString());
		req.setAttribute("totalInterest", propertyBond.getTotalInterest().toString());
		req.setAttribute("bondcost", propertyBond.getBondCost().toString());
		req.setAttribute("transfercost", propertyBond.getTransferCost().toString());
		req.setAttribute("legalcost", propertyBond.getLegalCost().toString());
	}

	private Long generateCode() {
		return (long) (Math.random() * 1_000_000);
	}

	private PropertyBond createPropertyBond(HttpServletRequest req) {
		Long code = generateCode();
		Money price = new Money(Double.valueOf(req.getParameter("price")));
		Integer term = Integer.valueOf(req.getParameter("term"));
		Interest rate = new Interest(Double.valueOf(req.getParameter("interest")));
		Money deposit = new Money(Double.valueOf(req.getParameter("deposit")));
		PropertyBond propertyBond = new PropertyBond(code, price, term, rate, deposit);
		return propertyBond;
	}

	public String updatePropertyBond(HttpServletRequest req) {
		return "";
	}

	public String removePropertyBond(HttpServletRequest req) {
		try {
			PropertyBond propertyBond = getPropertyBond(req);
			propertyBondService.removePropertyBond(propertyBond);
			req.setAttribute("message", "Property bond successfully removed");
			req.setAttribute("investments", investmentService.getAllInvestments());
			req.setAttribute("bonds", propertyBondService.getAllPropertyBonds());
			return "/WEB-INF/views/index.jsp";
		}
		catch (Exception ex) {
			return "/WEB-INF/views/error.jsp";
		}
	}

	public String generatePropertyBondForecast(HttpServletRequest req) {
		PropertyBond propertyBond = null;
		List<Forecast> forecasts = null;
		if (req.getParameter("price") == null) {
			propertyBond = getPropertyBond(req);
		} else {
			propertyBond = createPropertyBond(req);
			Event event = createEvent(req);
			if (event != null) {
				propertyBond.addEvent(event);
			}
			List<Event> events = createEvents(req);
			if (events.size() > 0) {
				for (Event event2 : events) {
					propertyBond.addEvent(event2);
				}
			}
		}
		forecasts = propertyBondService.generateForecast(propertyBond);
		req.setAttribute("forecasts", forecasts);
		setAttributes(req, propertyBond);
		return "/WEB-INF/views/bond_forecast.jsp";

	}

	private Event createEvent(HttpServletRequest req) {
		if (req.getParameter("eventType") != null) {
			if (req.getParameter("eventValue").equals("") || req.getParameter("eventMonth").equals("")) {
				return null;
			} else {
				String eventType = req.getParameter("eventType");
				Integer eventMonth = Integer.parseInt(req.getParameter("eventMonth"));
				Double eventValue = Double.parseDouble(req.getParameter("eventValue"));
				EventType type = getEventType(eventType);
				return new Event(eventMonth, eventValue, type);
			}
		}
		return null;
	}

	private PropertyBond getPropertyBond(HttpServletRequest req) {
		Long code = Long.parseLong(req.getParameter("code"));
		return propertyBondService.getPropertyBondByCode(code);
	}

	private Double removeCommas(String amount) {
		Double newamount = 0.0;
		if (amount.contains(",")) {
			newamount = Double.parseDouble(amount.replace(",", ""));
			return newamount;
		}
		return Double.parseDouble(amount);
	}
}
