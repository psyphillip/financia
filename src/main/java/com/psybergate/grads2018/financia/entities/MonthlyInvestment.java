package com.psybergate.grads2018.financia.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MonthlyInvestment")
public class MonthlyInvestment extends Investment {

	public MonthlyInvestment() {
		super();
	}

	public MonthlyInvestment(Integer term, Interest interest, Money amount) {
		super(term, interest, amount);
	}
 
	public MonthlyInvestment(Long code, Integer term, Interest interest, Money amount) {
		super(code, term, interest, amount);
	}


	public List<Forecast> getForecasts() {
		List<Forecast> forecasts = new ArrayList<>();
		Money start = new Money(0.0);
		Interest interest = getInterest();
		for (int i = 1; i <= getTerm(); i++) {
			Money withdraw = new Money(0.0);
			Money changeAmount = getAmount();
			Money deposit = new Money(0.0);
			for (Event event : events) {
				if (i == event.getMonth()) {
					if (event.getEvent().equals(EventType.WITHDRAW)) {
						withdraw = new Money(event.getValue());
					}
					if (event.getEvent().equals(EventType.DEPOSIT)) {
						deposit = new Money(event.getValue());

					}
					if (event.getEvent().equals(EventType.CHANGE_INTEREST)) {
						interest = new Interest(event.getValue());

					}
					if (event.getEvent().equals(EventType.CHANGE_AMOUNT)) {
						changeAmount = new Money(event.getValue());
						setAmount(changeAmount);
					}
				}
			}
			Forecast forecast = new MonthlyForecast(start, interest, withdraw, deposit, changeAmount);
			forecasts.add(forecast);
			start = forecast.getTotal();
		}
		return forecasts;
	}

	public Money getTotalContribution() {
		List<Forecast> forecasts = getForecasts();
		Double total = 0.0;
		for (Forecast forecast : forecasts) {
			total += ((MonthlyForecast) forecast).getAmount().getAmount();
		}
		return new Money(total);
	}
}
