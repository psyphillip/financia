package com.psybergate.grads2018.financia.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 19 Jun 2018
 */
@Entity
public class Event {
 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Integer id;

	@Column
	private Integer month;

	@Column
	private Double value;

	@Column
	@Enumerated(EnumType.STRING)
	private EventType event;

	public Event() {

	}

	public Event(Integer month, Double value, EventType event) {
		super();
		this.month = month;
		this.value = value;
		this.event = event;
	}

	public Integer getMonth() {
		return month;
	}

	public Double getValue() {
		return value;
	}

	public EventType getEvent() {
		return event;
	}
 
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass().equals(obj.getClass())) {
			Event event = (Event) obj;
			if (event.getEvent().equals(this.event)) {
				if (event.getMonth() == this.month) {
					return true;
				}
			}
		}
		return false;
	}
 
}
