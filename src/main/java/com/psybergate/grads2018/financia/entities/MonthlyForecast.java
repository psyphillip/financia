package com.psybergate.grads2018.financia.entities;

public class MonthlyForecast extends Forecast {

	private Double amount;


	public MonthlyForecast(Money start, Interest interestRate, Money withdrawnAmount, Money depositAmount, Money amount) {
		super(start, interestRate, withdrawnAmount, depositAmount);
		this.amount = amount.getAmount();
	}
 
	public Money getInterestEarned() {
		interestEarned = (start + amount + depositAmount - withdrawnAmount) * (interestRate / 100 / 12);
		return new Money(interestEarned);
	}

	public Money getTotal() {
		total = start + amount + depositAmount - withdrawnAmount + getInterestEarned().getAmount();
		return new Money(total);
	}

	public Money getAmount() {
		return new Money(amount);
	}

}
