package com.psybergate.grads2018.financia.entities;

import com.psybergate.grads2018.financia.utilities.FinanciaBondUtility;

public class BondForecast extends Forecast {

	private Integer term;

	private Integer month = 1;

	private Double repayment;

	private Boolean fixed = false;

	public BondForecast(Interest interestRate, Money start, Integer term, Money repayment, Money withdraw, Money deposit,
			Integer month) {
		super(start, interestRate, withdraw, deposit);
		this.term = term;
		this.month = month;
		if (repayment.getAmount() > 0) {
			this.repayment = repayment.getAmount();
			fixed = true;
		}
		else {
			this.repayment = getRepayment().getAmount();
		}
	}

	@Override
	public Money getInterestEarned() {
		return new Money(FinanciaBondUtility.calcInterest(interestRate, start));
	}

	@Override
	public Money getTotal() {
		return new Money(
				FinanciaBondUtility.calcBalance(start, interestRate, (repayment - withdrawnAmount + depositAmount)));
	}

	public Money getRepayment() {
		if (fixed) {
			return new Money(this.repayment);
		}
		else {
			Double factor = getFactor();
			return new Money(FinanciaBondUtility.calcRepayment(start, factor));
		}
	}
 
	public Double getFactor() {
		return FinanciaBondUtility.calcFactorPerMonth(interestRate, term, month);
	}
}
