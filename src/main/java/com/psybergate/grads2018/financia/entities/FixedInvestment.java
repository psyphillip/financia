package com.psybergate.grads2018.financia.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("FixedInvestment")
public class FixedInvestment extends Investment {

	public FixedInvestment() {
		super();
	}

	public FixedInvestment(Integer term, Interest interest, Money amount) {
		super(term, interest, amount);
	}

	public FixedInvestment(Long code, Integer term, Interest interest, Money amount) {
		super(code, term, interest, amount);
	}
 
	public List<Forecast> getForecasts() {
		List<Forecast> forecasts = new ArrayList<>();
		Money start = getAmount();
		Interest interest = getInterest();
		for (int i = 1; i <= getTerm(); i++) {
			Money withdraw = new Money(0.0);
			Money deposit = new Money(0.0);
			for (Event event : events) {
				if (i == event.getMonth()) {
					if (event.getEvent().equals(EventType.WITHDRAW)) {
						withdraw = new Money(event.getValue());
					}
					if (event.getEvent().equals(EventType.DEPOSIT)) {
						deposit = new Money(event.getValue());

					}
					if (event.getEvent().equals(EventType.CHANGE_INTEREST)) {
						interest = new Interest(event.getValue());
					}
				}
			}
			Forecast forecast = new FixedForecast(start, interest, withdraw, deposit);
			forecasts.add(forecast);
			start = forecast.getTotal();
		}
		return forecasts;
	}

}
