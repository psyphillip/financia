package com.psybergate.grads2018.financia.entities;

public abstract class Forecast {

	protected Double start;

	protected Double interestEarned;

	protected Double total;

	protected Double interestRate;

	protected Double withdrawnAmount = 0.0;

	protected Double depositAmount = 0.0;
 
	public Forecast(Interest interestRate, Money start) {
		this.start = start.getAmount();
		this.interestRate = interestRate.getInterestRate();
	}
	

	public Forecast(Money start, Interest interestRate, Money withdrawnAmount,
			Money depositAmount) {
		super();
		this.start = start.getAmount();
		this.interestRate = interestRate.getInterestRate();
		this.withdrawnAmount = withdrawnAmount.getAmount();
		this.depositAmount = depositAmount.getAmount();
	}


	public Money getStart() {
		return new Money(start);
	}

	public abstract Money getInterestEarned();

	public abstract Money getTotal();

	public Interest getInterestRate() {
		return new Interest(interestRate);
	}

	public Money getDepositAmount() {
		return new Money(depositAmount);
	}

	public Money getWithdrawnAmount() {
		return new Money(withdrawnAmount);
	}

}
