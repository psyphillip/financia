package com.psybergate.grads2018.financia.entities;

import java.text.DecimalFormat;

public class Money {

	private final Double amount;

	private static final DecimalFormat FORMAT = new DecimalFormat("#,#00.00");

	public Money(Double amount) {
		this.amount = amount;
	}
 
	public Double getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return FORMAT.format(amount);
	}

}
