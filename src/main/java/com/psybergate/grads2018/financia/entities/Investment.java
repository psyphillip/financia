package com.psybergate.grads2018.financia.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.psybergate.grads2018.financia.exceptions.FinanciaException;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class Investment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Integer id;

	@Column(nullable = false, updatable = false, unique = true)
	private Long code;

	@Column(nullable = false)
	private Integer term;

	@Column(nullable = false)
	private Double interest;

	@Column(name = "amount", nullable = false)
	private Double amount;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "forecast_id")
	protected List<Event> events = new ArrayList<>();

	public Investment(Long code, Integer term, Interest interest, Money amount) {
		super();
		this.code = code;
		this.term = term;
		this.interest = interest.getInterestRate();
		this.amount = amount.getAmount();
	}
	

	public Investment() {

	}

	public Investment(Integer term, Interest interest, Money amount) {
		this.term = term;
		this.interest = interest.getInterestRate();
		this.amount = amount.getAmount();
	}

	public Money getTotalWithdrawnAmount() {
		Double total = 0.0;
		for (Event event : events) {
			if (event.getEvent().equals(EventType.WITHDRAW)) {
				total += event.getValue();
			}
		}
		return new Money(total);
	}

	public Money getTotalDepositedAmount() {
		Double total = 0.0;
		List<Forecast> forecasts = getForecasts();
		for (Forecast forecast : forecasts) {
			total += forecast.getDepositAmount().getAmount();
		}
		return new Money(total);
	}

	public Money getFinalClosingAmount() {
		List<Forecast> forecasts = getForecasts();
		Double total = forecasts.get(forecasts.size() - 1).getTotal().getAmount();
		return new Money(total);
	}

	public Money getTotalInterestEarned() {
		List<Forecast> forecasts = getForecasts();
		Double total = 0.0;
		for (Forecast forecast : forecasts) {
			total += forecast.getInterestEarned().getAmount();
		}
		return new Money(total);
	}

	public Long getCode() {
		return code;
	}

	public Integer getTerm() {
		return term;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Interest getInterest() {
		return new Interest(interest);
	}

	public Money getAmount() {
		return new Money(amount);
	}

	public String getInvestmentType() {
		return getClass().getSimpleName();
	}

	public void addEvent(Event event) {
		validateEvent(event);
		events.add(event);
	}

	public List<Event> getEvents() {
		return events;
	}

	private void validateEvent(Event event) {
		List<Forecast> forecasts = getForecasts();
		if (event.getMonth() < 0 || event.getMonth() > term)
			throw new FinanciaException("Event not added: month should be between 2 and " + term + ".");
		if (event.getEvent().equals(EventType.WITHDRAW)) {
			if (event.getValue() > forecasts.get(event.getMonth() - 1).getStart().getAmount()) {
				throw new FinanciaException("Can't withdraw amount you don't have.");
			}
		}
	}

	public abstract List<Forecast> getForecasts();

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass().equals(obj.getClass())) {
			Investment investment = (Investment) obj;
			if (term.equals(investment.term)) {
				if (interest.equals(investment.interest)) {
					if (amount.equals(investment.amount)) {
						if (investment.events.size() == events.size()) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public void setAmount(Money amount) {
		this.amount = amount.getAmount();
	}

	public void removeEvent(Event event) {
		events.remove(event);
	}

}
