package com.psybergate.grads2018.financia.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.psybergate.grads2018.financia.utilities.FinanciaBondUtility;
import com.psybergate.grads2018.financia.utilities.FinanciaTaxUtility;

@Entity
@Table(name = "property_bond")
public class PropertyBond {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Integer id;

	@Column(nullable = false, updatable = false, unique = true)
	private Long code;
 
	@Column(name = "property_price", nullable = false)
	private Double propertyPrice;

	@Column(nullable = false)
	private Integer term;

	@Column(nullable = false)
	private Double rate;

	@Column(nullable = false)
	private Double deposit;

	@Transient
	protected List<Forecast> forecasts = new ArrayList<>();

	@Transient
	protected String bondRepaymentTime;

	@Transient
	protected Money outstandingAmount;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	protected List<Event> events = new ArrayList<>();

	public PropertyBond() {
		super();
	}

	public PropertyBond(Long code, Money propertyPrice, Integer term, Interest rate, Money deposit) {
		super();
		this.code = code;
		this.propertyPrice = propertyPrice.getAmount();
		this.term = term;
		this.rate = rate.getInterestRate();
		this.deposit = deposit.getAmount();
		this.bondRepaymentTime = term.toString();
	}

	public String getBondRepaymentTime() {
		return bondRepaymentTime;
	}

	public Money getOutstandingAmount() {
		return outstandingAmount;
	}

	public Long getCode() {
		return code;
	}

	public Money getPropertyPrice() {
		return new Money(propertyPrice);
	}

	public void setPropertyPrice(Money propertyPrice) {
		this.propertyPrice = propertyPrice.getAmount();
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Interest getRate() {
		return new Interest(rate);
	}

	public void setRate(Interest rate) {
		this.rate = rate.getInterestRate();
	}

	public Money getDeposit() {
		return new Money(deposit);
	}

	public void setDeposit(Money deposit) {
		this.deposit = deposit.getAmount();
	}

	public Money getBondCost() {
		return new Money(FinanciaTaxUtility.calculateBondCost(propertyPrice));
	}

	public Money getLegalCost() {
		return new Money(FinanciaTaxUtility.calculateLegalCost(propertyPrice));
	}

	public Money getTransferCost() {
		return new Money(FinanciaTaxUtility.calculateTransferRates(propertyPrice));
	}

	public Money getRepayment() {
		Double start = FinanciaBondUtility.calcStartAmount(propertyPrice, deposit);
		Double factor = FinanciaBondUtility.calcFactor(rate, term);

		return new Money(FinanciaBondUtility.calcRepayment(start, factor));
	}

	public Money getTotalRepayment() {
		Double total = 0.0;
		for (Forecast forecast : forecasts) {
			total += ((BondForecast)forecast).getRepayment().getAmount();
		}
		return new Money(total);
	}

	public Money getTotalInterest() {
		Double totalInterest = 0.0;
		for (Forecast forecast : forecasts) {
			totalInterest += forecast.getInterestEarned().getAmount();
		}
		return new Money(totalInterest);
	}

	public void addEvent(Event event) {
		events.add(event);
	}

	public void removeEvent(Event event) {
		events.remove(event);
	}

	public List<Event> getEvents() {
		return events;
	}

	public List<Forecast> getForecasts() {
		Money start = new Money(FinanciaBondUtility.calcStartAmount(propertyPrice, deposit));
		Interest interest = getRate();
		Money fixed = null;

		for (int i = 1; i <= getTerm(); i++) {
			Money withdraw = new Money(0.0);
			Money deposit = new Money(0.0);
			Double factor = FinanciaBondUtility.calcFactorPerMonth(rate, term, i);
			Money repayment = new Money(FinanciaBondUtility.calcRepayment(start.getAmount(), factor));
			for (Event event : events) {
				if (i == event.getMonth()) {
					if (event.getEvent().equals(EventType.WITHDRAW)) {
						withdraw = new Money(event.getValue());
					}
					if (event.getEvent().equals(EventType.DEPOSIT)) {
						deposit = new Money(event.getValue());

					}
					if (event.getEvent().equals(EventType.CHANGE_INTEREST)) {
						interest = new Interest(event.getValue());
						rate = interest.getInterestRate();
					}
					if (event.getEvent().equals(EventType.ADD_FIXED_REPAYMENT)) {
						fixed = new Money(event.getValue());
					}
				}
			}
			if (fixed != null) {
				repayment = fixed;
			}
			if (start.getAmount() < repayment.getAmount()) {
				bondRepaymentTime = i + "";
				Forecast temp = new BondForecast(interest, start, term, repayment, withdraw, deposit, i);
				repayment = new Money(start.getAmount() + temp.getInterestEarned().getAmount());
				Forecast forecast = new BondForecast(interest, start, term, repayment, withdraw, deposit, i);
				forecasts.add(forecast);
				start = new Money(0.0);
				break;
			}
			else {
				Forecast forecast = new BondForecast(interest, start, term, repayment, withdraw, deposit, i);
				forecasts.add(forecast);
				start = new Money(forecast.getTotal().getAmount());
			}
		}
		outstandingAmount = start;
		if (outstandingAmount.getAmount() > 0) {
			bondRepaymentTime = "NOT REPAID IN TIME";
		}
		return forecasts;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass().equals(obj.getClass())) {
			PropertyBond bond = (PropertyBond) obj;
			if (term.equals(bond.term)) {
				if (rate.equals(bond.rate)) {
					if (deposit.equals(bond.deposit)) {
						if (propertyPrice.equals(bond.propertyPrice)) {
							if (events.size() == bond.events.size()) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

}
