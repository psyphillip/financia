package com.psybergate.grads2018.financia.entities;

/**
 * 25 Jun 2018
 */

public enum EventType {
	WITHDRAW,
	DEPOSIT,
	CHANGE_AMOUNT,
	CHANGE_INTEREST,
	ADD_FIXED_REPAYMENT;

}

 