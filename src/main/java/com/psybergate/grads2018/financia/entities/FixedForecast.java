package com.psybergate.grads2018.financia.entities;

public class FixedForecast extends Forecast {

	public FixedForecast(Money start, Interest interestRate, Money withdrawnAmount, Money depositAmount) {
		super(start, interestRate, withdrawnAmount, depositAmount);
	}

	public Money getInterestEarned() {
		interestEarned = (start + depositAmount - withdrawnAmount) * (getInterestRate().getInterestRate() / 100 / 12);
		return new Money(interestEarned);
	}
 
	public Money getTotal() {
		total = start + depositAmount - withdrawnAmount + getInterestEarned().getAmount();
		return new Money(total);
	}

}
