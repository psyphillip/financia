package com.psybergate.grads2018.financia.entities;

import java.text.DecimalFormat;

public class Interest {

	private final Double interestRate;

	private static final DecimalFormat FORMAT = new DecimalFormat("#0.00");

	public Interest(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Double getInterestRate() {
		return interestRate;
	}
 
	@Override
	public String toString() {
		return FORMAT.format(interestRate);
	}

}
