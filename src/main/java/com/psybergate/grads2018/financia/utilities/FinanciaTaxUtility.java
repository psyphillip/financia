package com.psybergate.grads2018.financia.utilities;

public class FinanciaTaxUtility {

	private static final Double LEGAL_COST_PERCENTAGE = 0.012;
	private static final Double BOND_COST_PERCENTAGE = 0.01;

	private static final int FIRST_MAX = 10_500;
	private static final int SECOND_MAX = 40_500;
	private static final int THIRD_MAX = 80_500;
	private static final int FOURTH_MAX = 933_000;
	
	private static final int FIRST_BRACKET = 900_000;
	private static final int SECOND_BRACKET = 1_250_000;
	private static final int THIRD_BRACKET = 1_750_000;
	private static final int FOURTH_BRACKET = 2_250_000;
	private static final int FIFTH_BRACKET = 10_000_000;

	private static final double FIRST_PERCENTAGE = 0.03;
	private static final double SECOND_PERCENTAGE = 0.06;
	private static final double THIRD_PERCENTAGE = 0.08;
	private static final double FOURTH_PERCENTAGE = 0.11;
	private static final double FIFTH_PERCENTAGE = 0.13;

	public static Double calculateTransferRates(double propertyPrice) {
		Double result = 0.0;
		Double tempPrice = 0.0;

		if (propertyPrice > 0 && propertyPrice <= FIRST_BRACKET) {
			return result;
		} 
		else if (propertyPrice > FIRST_BRACKET && propertyPrice <= SECOND_BRACKET) {
			tempPrice = (double) (propertyPrice - FIRST_BRACKET);
			result = FIRST_PERCENTAGE * tempPrice;
		}
		else if (propertyPrice > SECOND_BRACKET && propertyPrice <= THIRD_BRACKET) {
			tempPrice = (double) (propertyPrice - SECOND_BRACKET);
			result = (SECOND_PERCENTAGE * tempPrice) + FIRST_MAX;
		}
		else if (propertyPrice > THIRD_BRACKET && propertyPrice <= FOURTH_BRACKET) {
			tempPrice = (double) (propertyPrice - THIRD_BRACKET);
			result = (THIRD_PERCENTAGE * tempPrice) + SECOND_MAX;
		}
		else if (propertyPrice > FOURTH_BRACKET && propertyPrice <= FIFTH_BRACKET) {
			tempPrice = (double) (propertyPrice - FOURTH_BRACKET);
			result = (FOURTH_PERCENTAGE * tempPrice) + THIRD_MAX;
		}
		else if (propertyPrice > FIFTH_BRACKET) {
			tempPrice = (double) (propertyPrice - FIFTH_BRACKET);
			result = (FIFTH_PERCENTAGE * tempPrice) + FOURTH_MAX;
		}
		return result;
	}
	
	public static Double calculateBondCost(Double propertyPrice) {
		return propertyPrice * BOND_COST_PERCENTAGE;
	}

	public static Double calculateLegalCost(Double propertyPrice) {
		return propertyPrice * LEGAL_COST_PERCENTAGE;
	}

}
