package com.psybergate.grads2018.financia.utilities;

public class FinanciaBondUtility {

	public static Double calcStartAmount(Double propertyPrice, Double deposit) {
		return propertyPrice - deposit;
	}

	public static Double calcInterest(Double interestRate, Double start) {
		return ((interestRate / 100) * start) / 12;
	}

	public static Double calcRepayment(Double start, Double factor) {
		return (start / 1000) * factor;
	}

	public static Double calcBalance(Double start, Double interestRate, Double repayment) {
		return start + calcInterest(interestRate, start) - repayment;
	}

	public static Double calcFactor(Double interestRate, Integer term) {
		double interest = ((interestRate / 100) / 12);
		double denominator = Math.pow((1 /(1 + interest)), term);
		return (interest / (1 - denominator)) * 1000;
	}
	
	public static Double calcFactorPerMonth(Double interestRate, Integer term, Integer month) {
		double interest = ((interestRate / 100) / 12);
		double denominator = Math.pow((1 /(1 + interest)), term-month+1);
		return (interest / (1 - denominator)) * 1000;
	}
}
