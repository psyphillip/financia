package com.psybergate.grads2018.financia.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.PropertyBond;
import com.psybergate.grads2018.financia.exceptions.FinanciaException;
import com.psybergate.grads2018.financia.resources.PropertyBondResource;

@Stateless
public class PropertyBondServiceImpl implements PropertyBondService {

	@Inject
	private PropertyBondResource propertyBondResource;

	@Override
	public void savePropertyBond(PropertyBond propertyBond) {
		if (isValid(propertyBond)) {
			propertyBondResource.savePropertyBond(propertyBond);
		} else {
			throw new FinanciaException(getFieldMessageError(propertyBond));
		}
	}

	@Override
	public void updatePropertyBond(PropertyBond propertyBond) {
	}

	@Override
	public void removePropertyBond(PropertyBond propertyBond) {
		propertyBondResource.removePropertyBond(propertyBond);
	}

	@Override
	public PropertyBond getPropertyBondByCode(Long code) {
		return propertyBondResource.getPropertyBond(code);
	}

	@Override
	public List<PropertyBond> getAllPropertyBonds() {
		return propertyBondResource.getAllPropertyBonds(PropertyBond.class);
	}

	@Override
	public String getFieldMessageError(PropertyBond propertyBond) {
		if (propertyBond.getDeposit().getAmount() < 0) {
			return "Deposit  can not be less than 0";
		}
		if (propertyBond.getPropertyPrice().getAmount() <= 0) {
			return "Price can not be less than 0";
		}
		if (propertyBond.getRate().getInterestRate() <= 0) {
			return "Interest  can not be less than or equal to 0";
		}
		if (propertyBond.getTerm() <= 0) {
			return "Term  can not be less than or equeal to 0";
		}

		return "Something went wrong";
	}

	private boolean isValid(PropertyBond propertyBond) {
		if (propertyBond.getDeposit().getAmount() < 0 || propertyBond.getPropertyPrice().getAmount() <= 0
				|| propertyBond.getRate().getInterestRate() <= 0 || propertyBond.getTerm() <= 0) {
			return false;
		}

		return true;
	}

	@Override
	public List<Forecast> generateForecast(PropertyBond propertyBond) {
		return propertyBond.getForecasts();
	}
}
