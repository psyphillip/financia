package com.psybergate.grads2018.financia.services;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.Investment;

@Local
public interface InvestmentService {

	public void saveInvestment(Investment investment);

	public void updateInvestment(Investment investment);

	public void removeInvestment(Investment investment);

	public List<Investment> getAllInvestments();

	public Investment getInvestmentByCode(Long code);

	public List<Forecast> generateForecast(Investment investment);

	public void removeEvent(Investment investment, Event event);

	public void addEvent(Investment investment, Event event);
}
