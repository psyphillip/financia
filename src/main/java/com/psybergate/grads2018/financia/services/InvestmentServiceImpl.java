package com.psybergate.grads2018.financia.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.psybergate.grads2018.financia.entities.Event;
import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.Investment;
import com.psybergate.grads2018.financia.exceptions.FinanciaException;
import com.psybergate.grads2018.financia.resources.InvestmentResource;

@Stateless
public class InvestmentServiceImpl implements InvestmentService {

	@Inject
	private InvestmentResource investmentResource;

	@Override
	public void saveInvestment(Investment investment) {
		validateInvestment(investment);
		investmentResource.saveInvestment(investment);
	}


	@Override
	public void updateInvestment(Investment investment) {
		investmentResource.updateInvestment(investment);
	}

	@Override
	public void removeInvestment(Investment investment) {
		investmentResource.removeInvestment(investment);
	}

	@Override
	public List<Investment> getAllInvestments() {
		return investmentResource.getAllInvestments(Investment.class);
	}

	@Override
	public Investment getInvestmentByCode(Long code) {
		return investmentResource.getInvestmentByCode(code);
	}

	private void validateInvestment(Investment investment) {
		if (investment.getAmount().getAmount() <= 0) {
			throw new FinanciaException("The investment amount can't be less than R0.00.");
		}
		if (investment.getTerm() < 1) {
			throw new FinanciaException("The investment term can't be less than 1.");

		}
		if (investment.getInterest().getInterestRate() <= 0) {
			throw new FinanciaException("The investment interest can't be less than 0.0%.");
		}
	}

	public List<Forecast> generateForecast(Investment investment) {
		return investment.getForecasts();
	}

	@Override
	public void removeEvent(Investment investment, Event event) {
		investment.removeEvent(event);
	}

	@Override
	public void addEvent(Investment investment, Event event) {
		investment.addEvent(event);
	}
}
