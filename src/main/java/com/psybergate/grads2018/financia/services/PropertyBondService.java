package com.psybergate.grads2018.financia.services;

import java.util.List;

import javax.ejb.Local;

import com.psybergate.grads2018.financia.entities.Forecast;
import com.psybergate.grads2018.financia.entities.PropertyBond;

@Local
public interface PropertyBondService {

	public void savePropertyBond(PropertyBond propertyBond);

	public void updatePropertyBond(PropertyBond propertyBond);

	public void removePropertyBond(PropertyBond propertyBond);
	
	public PropertyBond getPropertyBondByCode(Long code);
	
	public List<PropertyBond> getAllPropertyBonds();

	public String getFieldMessageError(PropertyBond propertyBond);

	public List<Forecast> generateForecast(PropertyBond propertyBond);
	
}
