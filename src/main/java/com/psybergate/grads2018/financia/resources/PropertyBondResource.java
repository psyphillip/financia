package com.psybergate.grads2018.financia.resources;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.psybergate.grads2018.financia.entities.PropertyBond;

@Dependent
public class PropertyBondResource {

	@PersistenceContext(unitName = "FinanciaUnit")
	private EntityManager manager;

	public void savePropertyBond(PropertyBond propertyBond) {
		if (!getAllPropertyBonds(PropertyBond.class).contains(propertyBond)) {
			manager.persist(propertyBond);
		}
	}

	public void updatePropertyBond(PropertyBond propertyBond) {
	}

	public void removePropertyBond(PropertyBond propertyBond) {
		manager.remove(manager.merge(propertyBond));
	}

	public PropertyBond getPropertyBond(Long code) {
		return (PropertyBond) manager.createQuery("select i from PropertyBond i where i.code=" + code).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<PropertyBond> getAllPropertyBonds(Class<?> clazz) {
		return (List<PropertyBond>) manager.createQuery("from PropertyBond", clazz).getResultList();
	}

}
