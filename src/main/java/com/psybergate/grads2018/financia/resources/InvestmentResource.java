package com.psybergate.grads2018.financia.resources;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.psybergate.grads2018.financia.entities.Investment;

@Dependent
public class InvestmentResource {
 
	@PersistenceContext(unitName = "FinanciaUnit")
	private EntityManager manager;
 
	public void saveInvestment(Investment investment) {
		if (!getAllInvestments(Investment.class).contains(investment)) {
			manager.persist(investment);
		}
	}

	public void updateInvestment(Investment investment) {
		manager.persist(manager.merge(investment));
	}

	public void removeInvestment(Investment investment) {
		manager.remove(manager.merge(investment));

	}

	@SuppressWarnings("unchecked")
	public List<Investment> getAllInvestments(Class<?> clazz) {
		return (List<Investment>) manager.createQuery("from Investment", clazz).getResultList();
	}

	public Investment getInvestmentByCode(Long code) {
		return (Investment) manager.createQuery("select i from Investment i where i.code=" + code).getSingleResult();
	}
}
