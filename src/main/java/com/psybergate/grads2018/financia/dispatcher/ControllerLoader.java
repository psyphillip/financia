package com.psybergate.grads2018.financia.dispatcher;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class ControllerLoader {

	private static Map<String, Object> controllers = new HashMap<>();

	public static void registerControllers() {
		try (InputStream is = ControllerLoader.class.getClassLoader().getResourceAsStream("controllers.properties")) {
			Properties props = new Properties();
			props.load(is);
			for (Object key : props.keySet()) {
				Object controller = (Object) new InitialContext().lookup(props.getProperty(key.toString()));
				controllers.put(key.toString(), controller);
			}
			
		} catch (Exception ex) {
			throw new RuntimeException("Error - loading command properties", ex);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String invoke(HttpServletRequest req) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ServletException, IOException {
		String request = req.getParameter("request");
		String controllerClass = request.substring(0, request.indexOf(","));
		String methodStr = request.substring(request.indexOf(",")+1);
		try {
			Object controller = controllers.get(controllerClass);
			Class clazz = controller.getClass();
			Method method = clazz.getMethod(methodStr, HttpServletRequest.class);
			return (String) method.invoke(controller, req);
		}
		catch(Exception e) {
			throw new RuntimeException("No such controller.", e);
		}
	}
	
	public static Map<String, Object> getControllers() {
		return controllers;
	}
}
