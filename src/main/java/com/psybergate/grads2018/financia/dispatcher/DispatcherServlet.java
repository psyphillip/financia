package com.psybergate.grads2018.financia.dispatcher;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.psybergate.grads2018.financia.exceptions.FinanciaException;

@WebServlet(urlPatterns = { "/financia",})
public class DispatcherServlet extends HttpServlet {

	private static final long serialVersionUID = 4341731330270887461L;

	@Override
	public void init() throws ServletException {
		super.init();
		ControllerLoader.registerControllers();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Object obj = ControllerLoader.getControllers().get("investment");
		Method method = null;
		String invoke = "";
		try {
			method = obj.getClass().getMethod("getAllInvestments",HttpServletRequest.class);
			invoke = (String) method.invoke(obj,req);
			req.getRequestDispatcher(invoke).forward(req, resp);
		} catch (Exception e) {
			req.getRequestDispatcher(invoke).forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String jspPath = ControllerLoader.invoke(req);
			req.getRequestDispatcher(jspPath).forward(req, resp);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			PrintWriter out = resp.getWriter();
			resp.setContentType("text/html");
			out.write("<h3>Error 404</h3>");
		}
	}
}