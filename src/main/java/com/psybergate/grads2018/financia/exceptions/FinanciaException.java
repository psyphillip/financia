package com.psybergate.grads2018.financia.exceptions;

public class FinanciaException extends RuntimeException {

	private static final long serialVersionUID = 8079206909541776404L;
	
	private Exception cause;
	
	@SuppressWarnings("unused")
	private String message;

	public FinanciaException(String message) {
		super(message);
		this.message = message;
	}

	public FinanciaException(String message, Exception cause) {
		super(message, cause);
		this.cause = cause;
		this.message = message;
	}
	public Exception getCause() {
		return cause;
	}
}
