
<%@page import="com.psybergate.grads2018.financia.entities.Money"%>
<%@page import="com.psybergate.grads2018.financia.entities.Event"%>
<%@page
	import="com.psybergate.grads2018.financia.entities.MonthlyForecast"%>
<%@page import="java.util.List"%>
<%@page import="com.psybergate.grads2018.financia.entities.Forecast"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<title>Financia</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
	integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
	
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js">
	
</script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js">
	
</script>
</head>
<body onload="load()">
	<script>
		var events = [];
		function save() {
			document.getElementById("request").value = "investment,saveInvestment";
		}
		function update() {
			document.getElementById("request").value = "investment,updateInvestment";
		}
		function setOption() {
			var type = '${type}';
			document.getElementById(type).selected = 'true';
			if (type == "Fixed") {
				document.getElementById("eventType").options[2].hidden = true;
			} else {
				document.getElementById("eventType").options[2].hidden = false;
			}
		}
		function load() {
			setOption();
			message()
		}

		function message() {
			var message = '${message}';
			if (message == "") {
				document.getElementById("messagediv").hidden = true;
			}
		}
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});

		function hideAmount() {
			var type = document.getElementById("typeddid").value;

			if (type == "Fixed") {
				document.getElementById("eventType").options[2].hidden = true;
			} else {
				document.getElementById("eventType").options[2].hidden = false;
			}
		}
		function removeEvent() {
			document.getElementById("request").value = "investment,removeEvent";
		}
		function myFunction() {
			var input, filter, table, tr, td, i;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("myTable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[0];
				if (td) {
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
		function addEvent(){
			document.getElementById("request").value = "investment,addEvent";			
		}
	</script>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
		<form action="financia" method="post" style="height: 40px">
			<input type="hidden" name="request" height="0"
				value="investment,getAllInvestments"> <a href="financia"><img
				class="navbar-brand" width="80" height="60" src="img/logo.png"></img></a>
		</form>
	</nav>

	<div class="container"
		style="margin-top: 80px; width: 80%; height: 100%;" align="center">
		<div class="alert alert-warning alert-dismissible" id="messagediv"
			style="height: 45px">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>${message}</strong>
		</div>

		<table class="table table-borderless">
			<form action="financia" method="post" id="mainForm">
				<input id="hiddenText" name="eventList" type="hidden">
				<tr>
					<td><select id="typeddid" class="form-control"
						onChange=hideAmount() name="type" style="width: 105px;">
							<option id="Fixed">Fixed</option>
							<option id="Monthly">Monthly</option>
					</select></td>
					<td><input placeholder="Amount" class="form-control"
						type="number" name="amount" min="0" step=".01" required
						value="${amount}"></td>
					<td><input placeholder="Interest rate" class="form-control"
						type="number" name="interest" min="0" step=".01" required
						value="${interest}"></td>
					<td><input placeholder="Term (Months)" class="form-control"
						type="number" name="term" required value="${term}" min="1"></td>
					<td><input type="submit" class="btn btn-success"
						value="Generate"></td>
					<td><input type="submit" id="btnsave" class="btn btn-warning"
						value="Save" onclick="save()" style="color: white;"></td>
					<input type="hidden" id="request" name="request"
						value="investment,generateForecast" height="0">
			</form>
			<td><button class="btn btn-info" data-target="#eventmodal"
					value="Add event" style="color: white;" data-toggle="modal">Add
					event</button></td>
			</tr>
		</table>
		<div align="center" style="border-style: solid; border-color: orange;"
			class="modal fade" id="eventmodal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="addedid" style="color: green">Event</h3>
						<button type="button" class="close" data-dismiss="modal">&times;</button>

					</div>
					<div class="modal-body">
						<form>
							<table class="table table-borderless">

								<tr>
									<td><select class="form-control" id="eventType"
										name="eventType" form="mainForm">
											<option>Withdraw</option>
											<option>Deposit</option>
											<option>Change amount</option>
											<option>Change interest</option>
									</select></td>
									<td><input id="monthid" type="number" form="mainForm"
										name="eventMonth" class="form-control" placeholder="Month"
										min="2"></td>
									<td><input id="amountid" placeholder="Value"
										form="mainForm" class="form-control" type="number"
										name="eventValue"  min="0" step=".01"></td>
								</tr>
							</table>
						</form>
					</div>
					<div class="modal-footer">
						<input type="submit" form="mainForm" class="btn btn-warning"
							value="Add Event" onclick="addEvent()">
					</div>
				</div>
			</div>
		</div>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapse5"
							href="collapse5"
							style="display: block; height: 50px; width: 100%; background-color: #A6BCCC; text-decoration: none; color: white;">Investment
							Events</a>
					</h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse">
					<table class="table">
						<thead>
							<tr>
								<th>Month</th>
								<th>Event</th>
								<th>Value</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<%
								String htmlEvents = "";
								List<Event> events = (List<Event>) request.getAttribute("events");
								System.out.print("Event: " + events);
								if (events != null) {
									for (Event event : events) {
										htmlEvents += "<form><input type='hidden' name='request' value='investment,removeEvent'><tr><td><input  style='background: inherit; border: none; size:inherit; max-width: inherit'  type='text' form='mainForm' name='monthName' value='"
												+ event.getMonth()
												+ "' readonly></td><td><input  style='background: inherit; border: none; size:inherit; max-width: inherit'  type='text' form='mainForm' name='eventName' value='"
												+ event.getEvent().toString()
												+ "' readonly></td><td><input  style='background: inherit; border: none; size:inherit; max-width: inherit'  type='text' form='mainForm' name='valueName' value='"
												+ new Money(event.getValue()).toString()
												+ "' readonly></td><td align='left'><Button data-toggle='tooltip' data-placement='top' title='Delete Event.'  onclick='removeEvent()' type='submit' form='mainForm' class='btn btn-danger far fa-trash-alt'></Button></td></tr><form>";
									}
								}
								request.setAttribute("htmlEvents", htmlEvents);
							%>
							${htmlEvents}
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#collapse1"
							href="collapse1"
							style="display: block; height: 50px; width: 100%; background-color: #A6BCCC; text-decoration: none; color: white;">Investment
							Summary</a>
					</h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">
					<table class="table">
						<thead>
							<tr align="right">
								<th>Total Deposits</th>
								<th>Total Withdrawals</th>
								<th>Total Contributions(Monthly Only)</th>
								<th>Total Interest Earned</th>
								<th>Final Closing Balance</th>
							</tr>
						</thead>
						<tr align="right">
							<td>${totalDeposit}</td>
							<td>${totalWithdrawals}</td>
							<td>${totalContributions}</td>
							<td>${totalInterest}</td>
							<td>${finalClosingBalance}</td>
						</tr>
					</table>

				</div>
			</div>
		</div>

		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-target="#stats" href="stats"
							style="display: block; height: 50px; width: 100%; background-color: #A6BCCC; text-decoration: none; color: white;">Investment
							Growth Graph</a>
					</h4>
				</div>
				<div id="stats" class="panel-collapse collapse">
					<div id="comparison_chart"></div>
				</div>
			</div>

		</div>

		<div class="pre-scrollable"
			style="border-style: solid; border-color: orange; height: 350px;">
			<h3>${type}&nbspGenerated&nbspForecast</h3>
			<form>
				<input onkeyup="myFunction()" type="number" id="myInput"
					class="form-control fal fa-search" placeholder="Search month.."
					style="background-image: url('img/search.png'); background-size: 20px; background-position: 10px 12px; background-repeat: no-repeat; font-size: 16px; padding: 12px 20px 12px 40px;">
				<table class="table table-striped" id="myTable">
					<%
						String htmlTable = "";
						String investments = "";
						int index = 1;
						if (request.getAttribute("forecasts") != null) {
							List<Forecast> forecasts = (List<Forecast>) request.getAttribute("forecasts");
							if (request.getAttribute("type").equals("Fixed")) {
								htmlTable += "<thead align='right'><tr><th>Month</th><th>Opening balance</th><th>Interest Rate</th><th>Amount deposited</th><th>Amount withdrawn</th><th>Interest</th><th>Closing balance</th></tr></thead><tbody align='right'>";
								for (Forecast forecast : forecasts) {
									investments += forecast.getTotal().getAmount() + "--";
									htmlTable += "<tr><td>" + index + "</td><td>" + forecast.getStart().toString() + "</td><td>"
											+ forecast.getInterestRate().toString() + "</td><td>"
											+ forecast.getDepositAmount().toString() + "</td><td>"
											+ forecast.getWithdrawnAmount().toString() + "</td><td>"
											+ forecast.getInterestEarned().toString() + "</td><td>" + forecast.getTotal().toString()
											+ "</td></tr>";
									index++;
								}
								htmlTable += "</tbody>";
							} else {
								htmlTable += "<thead align='right'><tr><th>Month</th><th>Opening balance</th><th>Monthly amount</th><th>Amount deposited</th><th>Amount withdrawn</th><th>Interest rate</th><th>Interest</th><th>Closing balance</th></tr></thead><tbody align='right'>";
								for (Forecast forecast : forecasts) {
									investments += forecast.getTotal().getAmount() + "--";
									htmlTable += "<tr><td>" + index + "</td><td>" + forecast.getStart().toString() + "</td><td>"
											+ ((MonthlyForecast) forecast).getAmount().toString() + "</td><td>"
											+ forecast.getDepositAmount().toString() + "</td><td>"
											+ forecast.getWithdrawnAmount().toString() + "</td><td>"
											+ forecast.getInterestRate().toString() + "</td><td>"
											+ forecast.getInterestEarned().toString() + "</td><td>" + forecast.getTotal().toString()
											+ "</td></tr>";
									index++;
								}
								htmlTable += "</tbody>";
							}
						}
						request.setAttribute("htmlTable", htmlTable);
						request.setAttribute("investmentList", investments);
					%>
					${htmlTable}
				</table>
				<label id="investmentList" hidden>${investmentList}</label>
			</form>
		</div>
	</div>
	<script type="text/javascript"
		src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {
			'packages' : [ 'corechart' ]
		});
		google.charts.setOnLoadCallback(drawChart);

		var investmentList = document.getElementById("investmentList").innerHTML;
		function drawChart() {
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Month');
			data.addColumn('number', 'Investment');
			var investments = investmentList.split("--");
			data.addRows(investments.length);

			for (var i = 0; i < investments.length; i++) {
				var number = Math.floor(parseFloat(investments[i]));
				data.setCell(i, 0, (i + 1) + "");
				data.setCell(i, 1, number);
			}

			var options = {
				title : 'Investment Growth',
				curveType : 'function',
				width : 1100,
				height : 300,
				legend : {
					position : 'bottom'
				}
			};

			var chart = new google.visualization.LineChart(document
					.getElementById('comparison_chart'));

			chart.draw(data, options);
		}
	</script>
</body>
</html>

