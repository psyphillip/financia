<%@page
	import="com.psybergate.grads2018.financia.services.InvestmentServiceImpl"%>
<%@page import="javax.ejb.EJB"%>
<%@page import="com.psybergate.grads2018.financia.entities.PropertyBond"%>
<%@page import="com.psybergate.grads2018.financia.entities.Investment"%>
<%@page
	import="com.psybergate.grads2018.financia.controllers.InvestmentController"%>
<%@page import="java.util.List"%>
<html lang="en">
<head>
<title>Financia</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
	integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
	
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js">
	
</script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js">
	
</script>
</head>
<body onload="load()">
	<script>
		function remove() {
			var x = document.getElementsByName("request");
			for (i = 0; x.length; i++) {
				x[i].value = "investment,removeInvestment";
			}
		}
		function view() {
			var x = document.getElementsByName("request");
			for (i = 0; x.length; i++) {
				x[i].value = "investment,generateForecast";
			}
		}

		function remove1() {
			var x = document.getElementsByName("request");
			for (i = 0; x.length; i++) {
				x[i].value = "property,removePropertyBond";
			}
		}
		function view1() {
			var x = document.getElementsByName("request");
			for (i = 0; x.length; i++) {
				x[i].value = "property,generatePropertyBondForecast";
			}
		}

		function load() {
			var message = '${message}';
			if (message == "") {
				document.getElementById("messagediv").hidden = true;
			}
		}
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
		<form action="financia" method="post" style="height: 40px">
			<input type="hidden" name="request" height="0"
				value="investment,getAllInvestments"> <a href="financia"><img
				class="navbar-brand" width="80" height="60" src="img/logo.png"></img></a>
		</form>
	</nav>
	<div class="container" style="margin-top: 80px; width: 80%;"
		align="center">
		<div class="alert alert-warning alert-dismissible" id="messagediv"
			style="height: 45px">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>${message}</strong>
		</div>
		<table class="table table-borderless">
			<tr>
				<td align="center">
					<button class="btn btn-warning" data-toggle="modal"
						style="color: white" data-target="#generateInvestmentModal">Generate
						Investment Forecast</button>
				</td>
				<td align="center">
					<button class="btn btn-warning" data-toggle="modal"
						data-target="#generateBondModal" style="color: white">Generate
						Bond Forecast</button>
				</td>
			</tr>
		</table>
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse1"
							style="display: block; height: 50px; width: 100%; background-color: #A6BCCC; text-decoration: none; color: white;">Investment
							Forecasts</a>
					</h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="pre-scrollable" align="center"
							style="border-style: solid; border-color: orange; height: 500px;">
							<table class="table table-striped">
								<thead align="right">
									<tr>
										<th>Code</th>
										<th>Type</th>
										<th>Initial/Monthly amount</th>
										<th>Interest Rate</th>
										<th>Term (Months)</th>
										<th>Events</th>
									</tr>
								</thead>
								<tbody align="right">
									<%
										String html = "";
										List<Investment> investments = (List<Investment>) request.getAttribute("investments");
										for (Investment investment : investments) {
											html += "<form method='post' action='financia'> <input type='hidden' name='code' value='" + investment.getCode()
													+ "'/><tr><input type='hidden' id='request' name='request'/><td>" + investment.getCode() + "</td><td>"
													+ investment.getInvestmentType().toString() + "</td><td  align='right'>" + investment.getAmount().toString()
													+ "</td><td align='right'>" + investment.getInterest().toString() + "</td><td align='right'>"
													+ investment.getTerm()
													+ "</td><td>"+investment.getEvents().size()+"</td><td><Button  data-toggle='tooltip' data-placement='top' title='View Forecast.'  type='submit' onclick='view()' class='btn btn-warning fas fa-eye'></Button></td><td><Button style='color: black;' data-toggle='tooltip' data-placement='top' title='Delete Forecast.' onclick='remove()' type='submit' class='btn btn-danger far fa-trash-alt'></Button></td></tr>"
													+ "</form>";
										}
										request.setAttribute("html", html);
									%>
									${html}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse2"
							style="display: block; height: 50px; width: 100%; background-color: #A6BCCC; text-decoration: none; color: white;">Property
							Bond Forecasts</a>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="pre-scrollable" align="center"
							style="border-style: solid; border-color: orange; height: 500px;">
							<table class="table table-striped">
								<thead align="right">
									<tr>
										<th>Code</th>
										<th>Purchase Price</th>
										<th>Interest Rate</th>
										<th>Deposit</th>
										<th>Term (Months)</th>
										<th>Events</th>
									</tr>
								</thead>
								<tbody align="right">
									<%
										String bondhtml = "";
										List<PropertyBond> bonds = (List<PropertyBond>) request.getAttribute("bonds");
										for (PropertyBond bond : bonds) {
											bondhtml += "<form method='post' action='financia'> <input type='hidden' name='code' value='" + bond.getCode()
													+ "'/><tr><input type='hidden' id='request' name='request'/><td>" + bond.getCode() + "</td><td>"
													+ bond.getPropertyPrice().toString() + "</td><td align='right'>"
													+ bond.getRate().toString() + "</td><td align='right'>"
													+ bond.getDeposit().toString() + "</td><td align='right'>" + bond.getTerm()
													+ "</td><td>"+bond.getEvents().size()+"</td><td><Button  data-toggle='tooltip' data-placement='top' title='View Forecast.' type='submit' onclick='view1()' class='btn btn-warning fas fa-eye'></Button></td><td><Button  data-toggle='tooltip' data-placement='top' title='Delete Forecast.'  onclick='remove1()' type='submit' class='btn btn-danger far fa-trash-alt'></Button></td></tr></form>";
										}
										request.setAttribute("bondhtml", bondhtml);
									%>
									${bondhtml}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div align="center" style="border-style: solid; border-color: orange;"
			class="modal fade" id="generateInvestmentModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" style="align: center">Generate
							Investment Forecast</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<form action="financia" method="post">
						<div class="modal-body">
							<table class="table">
								<tr>
									<td>Investment Type &nbsp</td>
									<td><select class="form-control" name="type">
											<option>Fixed</option>
											<option>Monthly</option>
									</select></td>
								</tr>
								<tr>
									<td>Amount:</td>
									<td><input class="form-control" type="number"
										name="amount" min="0" step=".01" required></td>
								</tr>
								<tr>
									<td>Interest Rate:</td>
									<td><input class="form-control" type="number"
										name="interest" min="0" step=".01" required></td>
								</tr>
								<tr>
									<td>Term (Months):</td>
									<td><input class="form-control" type="number" name="term"
										required min="1"></td>
								</tr>
							</table>
						</div>
						<input type="hidden" name="request"
							value="investment,generateForecast" />
						<div class="modal-footer">
							<input type="hidden" name="btngen" value="generate" /> <br>
							<input class="btn btn-warning" type="submit" value="Generate"
								style="margin-left: 800px; color: white" />
						</div>
					</form>
				</div>
			</div>
		</div>

		<div align="center" style="border-style: solid; border-color: orange;"
			class="modal fade" id="generateBondModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" style="align: center">Generate
							Property Bond Forecast</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<form action="financia" method="post">
						<div class="modal-body">
							<table class="table">
								<tr>
									<td>Purchase price:</td>
									<td><input class="form-control" type="number" name="price"
										min="0" step=".01" required></td>
								</tr>
								<tr>
									<td>Interest Rate:</td>
									<td><input class="form-control" type="number"
										name="interest" min="0" step=".01" required></td>
								</tr>
								<tr>
									<td>Term (Months):</td>
									<td><input class="form-control" type="number" name="term"
										required></td>
								</tr>
								<tr>
									<td>Deposit:</td>
									<td><input class="form-control" type="number"
										name="deposit" required></td>
								</tr>
							</table>
						</div>
						<input type="hidden" name="request"
							value="property,generatePropertyBondForecast" />
						<div class="modal-footer">
							<input type="hidden" name="btngen" value="generate" /> <br>
							<input class="btn btn-warning" type="submit" value="Generate"
								style="margin-left: 800px; color: white" />
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>

</body>
</html>
